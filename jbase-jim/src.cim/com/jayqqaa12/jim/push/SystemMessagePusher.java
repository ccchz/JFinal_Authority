 
package com.jayqqaa12.jim.push;

import com.farsunset.cim.nio.mutual.Message;
import com.farsunset.cim.nio.session.DefaultSessionManager;




/**
 * 
 * @author farsunset (3979434@qq.com)
 */
public class SystemMessagePusher  extends DefaultMessagePusher{

	 public static SystemMessagePusher    instance= new SystemMessagePusher(DefaultSessionManager.instance);
 
	/**
	 * Constructor.
	 */
	public SystemMessagePusher() {
		super();
	}
	
	public SystemMessagePusher(DefaultSessionManager instance)
	{
		this.setSessionManager(instance);
	}

	@Override
	public void pushMessageToUser(Message MessageMO){
		
		MessageMO.setSender("system");
		super.pushMessageToUser(MessageMO);
		
	}
}
