 
package com.jayqqaa12.jim.handler;

import org.apache.log4j.Logger;

import com.farsunset.cim.nio.constant.CIMConstant;
import com.farsunset.cim.nio.handle.CIMRequestHandler;
import com.farsunset.cim.nio.mutual.ReplyBody;
import com.farsunset.cim.nio.mutual.SentBody;
import com.farsunset.cim.nio.session.CIMSession;
import com.farsunset.cim.nio.session.DefaultSessionManager;

/**
 * 断开连接，清除session
 * 
 * @author
 */
public class SessionClosedHandler implements CIMRequestHandler {


	public ReplyBody process(CIMSession ios, SentBody message) {


		if(ios.getAttribute(CIMConstant.SESSION_KEY)==null)
		{
			return null;
		}
	    String account = ios.getAttribute(CIMConstant.SESSION_KEY).toString();
	    DefaultSessionManager.instance.removeSession(account);
	    
		return null;
	}
	
 
	
}